<?php
/**
 * Template Name: Home
 * Description: Home Page Template - see pages and partials
 */
// $start = TimberHelper::start_timer();
$context = Timber::get_context();


	// // post type locations
		$locationPosts = array(
		    // Get post type project
		    'post_type' => 'db_location',
		    // Get all posts
		    'posts_per_page' => 4,
		);

		$context['LOCATION_POSTS'] = Timber::get_posts( $locationPosts );
	// // end post type

	// post type post
		$latestPosts = array(
			// Get post type project
			'post_type' => 'post',
			// Get all posts
			'posts_per_page' => 10,
		);

		$context['LATEST_POSTS'] = Timber::get_posts( $latestPosts );
	// end post type

	// post type post
	$latestRecipePosts = array(
		// Get post type project
		'post_type' => 'db_recipe',
		// Get all posts
		'posts_per_page' => 6,
	);

	$context['LATEST_RECIPE_POSTS'] = Timber::get_posts( $latestRecipePosts );
// end post type

	// $context['timber_categories'] = Timber::get_terms('category', array('exclude'=>'1'));

Timber::render('pages/front-page.twig', $context);

// echo TimberHelper::stop_timer( $start);