<?php
/**
 * Single page template
 *
 * @package  WordPress
 * @subpackage  SageTimber
 * @since  SageTimber 0.1
 */


$context = Timber::get_context();

if ( is_singular('db_location') ) {

    Timber::render('pages/single-location.twig', $context);
    
} elseif ( is_singular('db_recipe') ) {

    Timber::render('pages/single-recipe.twig', $context);
    
} else {
	
    Timber::render('pages/single.twig', $context);

}


