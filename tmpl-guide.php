<?php
/**
 * Template Name: Restaurant Guide
 * Description: Restaurant Guide Page Template - see pages and partials
 */
// $start = TimberHelper::start_timer();
$context = Timber::get_context();

    $regionTerms = get_terms( 'region', array(
        'orderby' => 'name',
        'order'   => 'ASC',
        'hide_empty' => '1'
    ) );
        
    $context['REGION_TERMS'] = $regionTerms;

    $dietTerms = get_terms( 'diet_type', array(
        'orderby' => 'name',
        'order'   => 'ASC',
        'hide_empty' => '1'
    ) );
        
    $context['DIET_TERMS'] = $dietTerms;
    
Timber::render('pages/restaurant-guide.twig', $context);