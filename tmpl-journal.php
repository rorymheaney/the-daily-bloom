<?php
/**
 * Template Name: Journal
 * Description: Journal Page Template - see pages and partials
 */
// $start = TimberHelper::start_timer();
$context = Timber::get_context();

    // post type post
        $latestPosts = array(
            // Get post type project
            'post_type' => 'post',
            // Get all posts
            'posts_per_page' => 10,
        );

        $context['LATEST_POSTS'] = Timber::get_posts( $latestPosts );
    // end post type

    // get categories that aren't empty
    $categories = get_categories( array(
        'orderby' => 'name',
        'order'   => 'ASC',
        'hide_empty' => '1'
    ) );
        
    $context['categories'] = $categories;
    // end get
    
Timber::render('pages/journal.twig', $context);


