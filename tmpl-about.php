<?php
/**
 * Template Name: About
 * Description: About Page Template - see pages and partials
 */

$context = Timber::get_context();
    
Timber::render('pages/about.twig', $context);