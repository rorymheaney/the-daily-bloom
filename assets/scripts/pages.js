const COMMON_COMPONENTS = require('./components/Common');
const HOME_COMPONENTS = require('./components/Home');
const JOURNAL_COMPONENTS = require('./components/Journal');
const REST_API_COMPONENTS = require('./components/LocationRestFilter');
const REST_API_RECIPE_COMPONENTS = require('./components/RecipeRestFilter');
const RECIPE_POST = require('./components/Recipe');

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
	var FancySquares = {
		// All pages
		'common': {
	  		init: function() {
				// JavaScript to be fired on all pages

				$(document).foundation(); // Foundation JavaScript
				// console.log('testing console log');
				COMMON_COMPONENTS.rmh_dataTogglerClass();

				COMMON_COMPONENTS.rmh_toggleSearchBar();

	  		},
	  		finalize: function() {
				// JavaScript to be fired on all pages, after page specific JS is fired
	  		}
		},
		// Home page
		'home': {
	  		init: function() {
				// JavaScript to be fired on the home page
				HOME_COMPONENTS.rmh_featuredSlider();

				HOME_COMPONENTS.rmh_masonryPosts();

				HOME_COMPONENTS.rmh_checkIfYouScrolled();	

				HOME_COMPONENTS.rmh_revealModal();

		},
	  		finalize: function() {
				// JavaScript to be fired on the home page, after the init JS
	  		}
		},
		// page-template-tmpl-journal
		'page_template_tmpl_journal': {
	  		init: function() {
				HOME_COMPONENTS.rmh_featuredSlider();

				HOME_COMPONENTS.rmh_masonryPosts();

				JOURNAL_COMPONENTS.rmh_LoadJournalPosts();
	  		}
		},
		// page-template-tmpl-guide
		'page_template_tmpl_guide': {
	  		init: function() {
				REST_API_COMPONENTS.rmh_getData();
	  		}
		},
		// page-template-tmpl-recipies
		'page_template_tmpl_recipies': {
	  		init: function() {
				REST_API_RECIPE_COMPONENTS.rmh_getRecipeData();
	  		}
		},
		// single-db_recipe
		'single_db_recipe': {
	  		init: function() {
				RECIPE_POST.rmh_highlightsSlider();

				RECIPE_POST.rmh_printDirections();
	  		}
		}
	};

	// The routing fires all common scripts, followed by the page specific scripts.
	// Add additional events for more control over timing e.g. a finalize event
	var UTIL = {
		fire: function(func, funcname, args) {
			var fire;
			var namespace = FancySquares;
			funcname = (funcname === undefined) ? 'init' : funcname;
			fire = func !== '';
			fire = fire && namespace[func];
			fire = fire && typeof namespace[func][funcname] === 'function';

			if (fire) {
			  namespace[func][funcname](args);
			}
		},
		loadEvents: function() {
			// Fire common init JS
			UTIL.fire('common');

			// Fire page-specific init JS, and then finalize JS
			$.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
				UTIL.fire(classnm);
				UTIL.fire(classnm, 'finalize');
			});

			// Fire common finalize JS
			UTIL.fire('common', 'finalize');
		}
	};

	// Load Events
  	$(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
