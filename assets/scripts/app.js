// for ie
import "babel-polyfill";
// foundation zurb js
import '../../node_modules/foundation-sites/dist/js/foundation.js';
// owl
import 'owl.carousel';

// jquery cookie
import 'js-cookie';
window.fancySquareCookies = require('js-cookie');

// axios
window.axios = require('axios');

// pages
import './pages.js'

// console.log('console blah');
