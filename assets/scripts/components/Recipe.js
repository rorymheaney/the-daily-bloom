function rmh_highlightsSlider() {
    let $owl = $('[data-js="highlights-slider"]');
    
    $owl.owlCarousel({
        center: true,
        items: 1,
        loop: true,
        margin: 0,
        nav: true
    });
}

function rmh_printDirections(){
    let $printButton = $('[data-js="print"]');

    $printButton.on('click',function(){
        window.print();
    });
}

module.exports = {
    rmh_highlightsSlider,
    rmh_printDirections
};