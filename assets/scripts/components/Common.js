function rmh_dataTogglerClass(){
    let $header = $('[data-js="header-color"]'),
        $navBar = $('.header__top-bar'),
        $menuButton = $('.menu-icon');

    $header.on("on.zf.toggler", function(e) {
            $navBar.removeClass('hide');
            $menuButton.addClass('menu-icon--open');
        })
        .on("off.zf.toggler", function(e) {
            $navBar.addClass('hide');
            $menuButton.removeClass('menu-icon--open');
        });
}

function rmh_toggleSearchBar(){
    let $search = $('[data-js="search-bar"]'),
        $searchForm = $('.search-block'),
        $headerMain = $('[data-js="header-color"]');

    $search.on('click',function(){
        $searchForm.fadeToggle();
        $headerMain.toggleClass('header--search-bar');
    });
}

module.exports = {
    rmh_dataTogglerClass,
    rmh_toggleSearchBar
};
