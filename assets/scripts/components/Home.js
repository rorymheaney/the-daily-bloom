let rmh_didScroll = false,
    rmh_docElem = document.documentElement;


function rmh_featuredSlider() {
    let $rmh_sliderImage = $('.slider-img'),
        $owl = $('[data-js="featured-slider"]');
    
    function runCarousel(){
        $owl.owlCarousel({
            center: true,
            items:1,
            loop:true,
            margin:175,
            nav:true
        });
    }
    
    Foundation.onImagesLoaded($rmh_sliderImage, runCarousel);

    // if(Foundation.MediaQuery.atLeast('large')){
	// 	console.log('large!');
	// }
}

function rmh_masonryPosts(){

    let $mediaItemContainer = $( '#posts-entered' );
        $mediaItemContainer.masonry( {
            itemSelector : '.masonry',
            columnWidth: '.medium-6',
            horizontalOrder: true
        })

        $mediaItemContainer.imagesLoaded().progress( function() {
            $mediaItemContainer.masonry( 'reloadItems' );
            $mediaItemContainer.masonry( 'layout' );
        
        });
}

// add class on scroll
function rmh_checkIfYouScrolled() {
    window.addEventListener( 'scroll', function( event ) {
        if( !rmh_didScroll ) {
            rmh_didScroll = true;
            setTimeout( rmh_scrollPage, 250 );
        }
    }, false );
}

function rmh_scrollPage() {
    let sy = rmh_scrollY(),
        rmh_changeHeaderOn = 150,
        $headerMain = $('[data-js="header-color"]')
    
    if ( sy >= rmh_changeHeaderOn ) {
        $headerMain.addClass('header--bg-color');
    }
    else {
        $headerMain.removeClass('header--bg-color');
    }
    rmh_didScroll = false;
}

function rmh_scrollY() {
    return window.pageYOffset || rmh_docElem.scrollTop;
}

// reveal modal
function rmh_revealModal(){
    if (fancySquareCookies.get('modal_shown') === undefined) {
        fancySquareCookies.set('modal_shown', 'value', { expires: 7 });	
        $('#newsletterModal').foundation('open');
    }
}

module.exports = {
    rmh_featuredSlider,
    rmh_masonryPosts,
    rmh_checkIfYouScrolled,
    rmh_revealModal
};