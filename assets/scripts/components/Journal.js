const HOME_COMPONENTS = require('./Home');

let pageNumnber = 1;

function rmh_LoadJournalPosts (argument) {
	

	let $loadMoreButton = $('#data-parameters'),
		postCategory = '.post-category',
		loadMoreButtonID = '#data-parameters',
		$fsLadda = $( '.ladda-button' ).ladda(),
        activeButtonClass = 'button-type--loading',
        $postLoop = $('[data-js*="posts-container"]');

	// start click
	$('main').on('click', postCategory, function(){

        console.log(this);

        
        let form_data = new FormData,
            dataValues = $loadMoreButton.data(),
            $categoryID = $(this).data('category')
        
        // console.log($categoryID);


			for (var key in dataValues) {
			    // all_values.push([key, data[key]]);
			    // key[pagecount] = pageCount;
			    if( key === 'style' || key === 'ladda' || key === 'loading') {
			    	continue;
			    } else {
					form_data.append(key, dataValues[key]);
				}
			    // console.log(key, dataValues[key]);
            }
            form_data.append('pagecount', pageNumnber);
            form_data.append('cat', $categoryID);

			// console.log(dataValues);

		axios.post(ajaxurl, form_data)
			.then(function(response){
				console.log(response);
				console.log(response.data);

				let postData = response.data;

				if(postData === 'nomore'){
			    	// $loadMoreButton.fadeOut();
			    } else {

                    $postLoop.empty();

			    	for(var hp = 0; hp < postData.length; hp++){
						$postLoop.append(postData[hp].data);
						// $this.attr('data-pagecount', Number(pageCount)+1);
						// // console.log(postData[hp]);

                    }
                    
					HOME_COMPONENTS.rmh_masonryPosts();
                    // pageNumnber = pageNumnber + 1;
                    // console.log(pageNumnber);

					
			    }
                // pageNumnber = pageNumnber + 1;
                // console.log(pageNumnber);

			    $fsLadda.ladda( 'stop' ).removeClass(activeButtonClass);
			})
			.catch(function (response) {
		        //handle error
		        console.log(response);
		    });


            

	});
	// end click
}


module.exports = {
	rmh_LoadJournalPosts
};