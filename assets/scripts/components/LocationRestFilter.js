const RMH_REST_URL = $('body').data('rest');

// removes element from array if in array
function rmh_remove(array, element) {
    const index = array.indexOf(element);
    array.splice(index, 1);
}

function rmh_getData(){

    
    let restRegions = [],
        restRegionString,
        restDiets = [],
        restDietsString,
        pageNumber = 1,
        $fadeToggle = $('[data-js="fade-toggle"]');

    $('[data-js="clear-results"]').on('click',function(){          
        $('li.checked').trigger('click');
        $fadeToggle.fadeIn();
        // console.log('clicked');
    });

    let ax = axios.create({
        baseURL: RMH_REST_URL + 'wp/v2/db_location'
    });

    // console.log(ax);
      
    new Vue({
        el: '#app',
        
        data () {
            return {
                posts: [],
                isLoading: false,
                hasMorePages: false
                // term: '',
            };
        },

        computed:{

        },
        
        mounted () {
            this.isLoading = false;
            this.hasMorePages = false;
            this.getPosts();
            this.handleSearch();
            this.loadMoreData();
        },
        
        methods: {
            getRestData(restRegionString, restDietsString){
                let self = this;

                ax.get(RMH_REST_URL + 'wp/v2/db_location', {
                    params: {
                        'filter[region]': restRegionString,
                        'filter[diet_type]': restDietsString,
                        'per_page': 6,
                        'page': pageNumber
                    }
                })
                .then(function (response) {
                    // console.log(response);
                    self.$set(self, 'posts', response.data);
                    self.$set(self, 'isLoading', false);

                    if(response.headers["x-wp-totalpages"] > 1){
                        self.$set(self, 'hasMorePages', false);
                    } else {
                        self.$set(self, 'hasMorePages', true);  
                    }
                })
                .catch(function (error) {
                    // console.log(error);
                    alert('please clear your filters and try a new search')
                });
            },
            loadMoreData(){
                let self = this;

                $('#load-more').on('click',function(){
                    
                    pageNumber = pageNumber + 1;

                    ax.get(RMH_REST_URL + 'wp/v2/db_location', {
                        params: {
                            'filter[region]': restRegionString,
                            'filter[diet_type]': restDietsString,
                            'per_page': 6,
                            'page': pageNumber
                        }
                    })
                    .then(function (response) {
                        console.log(response.data);
                        // self.$push(self, 'posts', response.data);
                        // self.posts.push(response.data)

                        for (let index = 0; index < response.data.length; index++) {
                            const element = response.data[index];
                            // console.log(element);
                            self.posts.push(element)
                        }

                        self.$set(self, 'isLoading', false);

                        
                    })
                    .catch(function (error) {
                        // console.log(error);
                        alert('please clear your filters and try a new search')
                    });
                });

            },
            handleSearch() {
                
                let self = this;
                

                // on region click
                $('[data-js="get-region"]').on('click',function(){
                    let region = $(this).data('slug');
                    
                    self.$set(self, 'isLoading', true);
                    
                    $fadeToggle.fadeOut();

                    if($(this).hasClass('checked')){
                        rmh_remove(restRegions, region);
                        $(this).removeClass('checked')
                    } else {
                        if(!restRegions.includes(region)){
                            restRegions.push(region)
                        }
                        $(this).addClass('checked');
                    }

                    pageNumber = 1;
            
                    restRegionString = restRegions.join();
                    // console.log(restRegionString);
                    self.getRestData(restRegionString, restDietsString);
                        
                });
                
                // on diet click
                $('[data-js="get-diet"]').on('click',function(){
                    let diet = $(this).data('slug');

                    self.$set(self, 'isLoading', true);

                    $fadeToggle.fadeOut();

                    if($(this).hasClass('checked')){
                        rmh_remove(restDiets, diet);
                        $(this).removeClass('checked')
                    } else {
                        if(!restDiets.includes(diet)){
                            restDiets.push(diet)
                        }
                        $(this).addClass('checked');
                    }
                    
                    pageNumber = 1;
            
                    restDietsString = restDiets.join();
                    // console.log(restDietsString);
                    self.getRestData(restRegionString, restDietsString);
                });
                
            },
            getPosts() {
                let self = this;
                // console.log(self)
                self.getRestData(restRegionString, restDietsString);
  
            } 
        }
    })
}

module.exports = {
    rmh_getData
};