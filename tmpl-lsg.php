<?php
/**
 * Template Name: Living Style Guide
 * Description: Living Style Guide Page Template - see pages and partials
 */
// $start = TimberHelper::start_timer();
$context = Timber::get_context();


Timber::render('pages/lsg.twig', $context);

