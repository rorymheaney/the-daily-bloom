<?php
/**
 * Fancy Squares includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 */
$sage_includes = [
  'lib/timber.php',    // Timber setup
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/customizer.php', // Theme customizer
  'lib/extras/acf-options.php', // Theme customizer
  'lib/posts/general-enhancements.php', // WordPress / Theme ehancements
  'lib/api/load-more-posts.php', // load more posts
  'lib/api/instagram.php', // instragm
  'lib/api/better-featured-img.php', // add acf to api for images
  'lib/posts/comment-form.php' // comment form
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'fancysquares'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

// Timber::$cache = true;

// add_action( 'graphql_post_fields', function( $fields ) {
// 	$fields['image_preview'] = [
// 		'type' => \WPGraphQL\Types::string(),
// 		'description' => __( 'A field containg misc info' ),
// 		'resolve' => function( \WP_Post $post ) {
//       $id = get_field( 'image_preview' );
// 			return wp_get_attachment_url($id);
// 		},
//   ];
//   $fields['source_misc'] = [
// 		'type' => \WPGraphQL\Types::string(),
// 		'description' => __( 'A field containg misc info' ),
// 		'resolve' => function( \WP_Post $post ) {
// 			return get_field( 'source_misc' );
// 		},
// 	];

// 	return $fields;
// } );

use WPGraphQL\Types;
add_filter( 'graphql_object_fields', 'graphql_object_fields_func', 10, 2);
function graphql_object_fields_func($fields, $type_name )
{
	$fields['value'] = [
		'type' => Types::string(),
		'resolve' => function( array $field ) {
			return ($field['type'] == 'flexible_content') ? json_encode(get_field( $field['key'], $field['object_id'], true )) : get_field( $field['key'], $field['object_id'], true );
		},
	];
	return $fields;
}