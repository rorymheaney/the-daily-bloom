const config = require('../app.config')

/**
 * Internal application javascript files.
 * Supports ES6 by compiling scripts with Babel.
 */
module.exports = {
    test: /\.js$/,
    exclude: /node_modules/,
    use: [
        {
            loader: "babel-loader",
        },
        {
            loader: "eslint-loader"
        }
    ],
}
// exclude: function() {
//     return /node_modules/ &&
//         !/node_modules\/foundation-sites/;
// },