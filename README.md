# Twig WordPress

## Requirements

| Prerequisite    | How to check | How to install
| --------------- | ------------ | ------------- |
| PHP >= 7.1.x    | `php -v`     | [php.net](http://php.net/manual/en/install.php) |
| Node.js >= 6.11.x  | `node -v`    | [nodejs.org](http://nodejs.org/) |
| MySql >= 5.6.x  |  ---   | --- |

## Features

* [Timber](https://www.upstatement.com/timber/) a faster, easier and more powerful way to build themes, it basically makes sure you can use all the WordPress magic with TWIG 
* [Twig, because you're an adult and should use a templating engine](https://twig.symfony.com/)
* WebPack
* must follow WordPress ENQUE standards unless other wise discussed, no need for all that extra bulk in the JS file when misc js plugins can be inlisted when needed when they aren't across several pages 


### From the command line

* npm: `npm install -g npm@latest`
* watch: `npm run watch`
* build: `npm run build`
* prod: `npm run prod`


```

## Documentation

* will provide
