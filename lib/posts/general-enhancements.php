<?php 

//
// 
// on upload, automatically add image title and alt
// 
//
add_action( 'add_attachment', 'my_set_image_meta_upon_image_upload' );
function my_set_image_meta_upon_image_upload( $post_ID ) {

  // Check if uploaded file is an image, else do nothing

	if ( wp_attachment_is_image( $post_ID ) ) {

		$my_image_title = get_post( $post_ID )->post_title;

		// Sanitize the title:  remove hyphens, underscores & extra spaces:
		$my_image_title = preg_replace( '%\s*[-_\s]+\s*%', ' ',  $my_image_title );

		// Sanitize the title:  capitalize first letter of every word (other letters lower case):
		$my_image_title = ucwords( strtolower( $my_image_title ) );

		// Create an array with the image meta (Title, Caption, Description) to be updated
		// Note:  comment out the Excerpt/Caption or Content/Description lines if not needed
		$my_image_meta = array(
			'ID'    => $post_ID,      // Specify the image (ID) to be updated
			'post_title'  => $my_image_title,   // Set image Title to sanitized title
			// 'post_excerpt'  => $my_image_title,   // Set image Caption (Excerpt) to sanitized title
			// 'post_content'  => $my_image_title,   // Set image Description (Content) to sanitized title
		);

		// Set the image Alt-Text
		update_post_meta( $post_ID, '_wp_attachment_image_alt', $my_image_title );

		// Set the image meta (e.g. Title, Excerpt, Content)
		wp_update_post( $my_image_meta );

	} 
}

//
// 
// google maps api
// 
//
function my_acf_init() {	
	acf_update_setting('google_api_key', 'AIzaSyCpoc2-hfvtodCrDzYBnDII9P8eoEyWafw');
}

add_action('acf/init', 'my_acf_init');


//
// 
// make jpeg quality 100 be default, smush after
// 
//
add_filter('jpeg_quality', function($arg){return 100;});



//
// 
// add popular posts to context
// 
//
add_filter( 'timber_context', 'fancySquares_show_popular_posts'  );

function fancySquares_show_popular_posts( $context ) {
    $context['RMH_POPULAR_POSTS'] = Timber::get_posts( fancySquares_find_popular_posts() );
    return $context;
}

function fancySquares_find_popular_posts()
{
    $popularArgs = array(
        // Get post type project
		'post_type' => 'db_recipe',
		// Get all posts
		'posts_per_page' => 3,
		'suppress_filters' => false,
		'orderby' => 'post_views',
		'fields' => ''
    );

    return $popularArgs;
}


// 
// 
// set updated featured image w/ acf image
// 
// 
function acf_set_featured_image( $value, $post_id, $field  ){
	$id = $value;
	if( ! is_numeric( $id ) ){
		$data = json_decode( stripcslashes($id), true );
		$id = $data['cropped_image'];
	}
	update_post_meta( $post_id, '_thumbnail_id', $id );
	return $value;
}

// acf/update_value/name={$field_name} - filter for a specific field based on it's name
add_filter( 'acf/update_value/name=image_preview', 'acf_set_featured_image', 10, 3 );



//
// 
// enque scripts
// 
//
add_action( 'wp_enqueue_scripts', 'rmh_loadedScripts' );

function rmh_loadedScripts() {
   

    if(is_front_page()){
		wp_enqueue_script("imagesLoaded", get_template_directory_uri () . "/assets/vendors/imagesloaded.js",array("jquery"),'1.0.0', true);
        wp_enqueue_script("vendor_masonry", get_template_directory_uri () . "/public/vendors/masonry.pkgd.min.js",array("jquery"), '', true);
	}
	
	if(is_page( 115 )){
		wp_enqueue_script("imagesLoaded", get_template_directory_uri () . "/assets/vendors/imagesloaded.js",array("jquery"),'1.0.0', true);
		wp_enqueue_script("vendor_masonry", get_template_directory_uri () . "/public/vendors/masonry.pkgd.min.js",array("jquery"), '', true);
		wp_enqueue_style( 'lada-style', get_template_directory_uri() . '/assets/vendors/ladda.min.css' );
        wp_enqueue_script( 'spin-js', get_template_directory_uri() . '/assets/vendors/spin.min.js', array("jquery"), '1.0.0', true );
		wp_enqueue_script("lada-js", get_template_directory_uri () . "/assets/vendors/ladda.min.js",array("jquery"),'1.0.0', true);
	}

	
	
	if(is_page( 90 ) || is_page( 128 )){
		// wp_enqueue_script( 'vue-core', "https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js", array(), '2.5.16',true );
		wp_enqueue_script( 'vue-core', "https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.min.js", array(), '2.5.16',true );
	}
	
}

function replace_core_jquery_version() {
	wp_deregister_script( 'jquery-core' );
	wp_register_script( 'jquery-core', "https://code.jquery.com/jquery-3.1.1.min.js", array(), '3.1.1' );
	wp_deregister_script( 'jquery-migrate' );
	wp_register_script( 'jquery-migrate', "https://code.jquery.com/jquery-migrate-3.0.0.min.js", array(), '3.0.0' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );


//
// 
// clear transients automatically
// 
//
// if ( ! function_exists('purge_transients') ) {
// 	function purge_transients($older_than = '12 hours', $safemode = true) {
// 		global $wpdb;
// 		$older_than_time = strtotime('-' . $older_than);
// 		if ($older_than_time > time() || $older_than_time < 1) {
// 			return false;
// 		}
// 		$transients = $wpdb->get_col(
// 			$wpdb->prepare( "
// 					SELECT REPLACE(option_name, '_transient_timeout_', '') AS transient_name 
// 					FROM {$wpdb->options} 
// 					WHERE option_name LIKE '\_transient\_timeout\__%%'
// 						AND option_value < %s
// 			", $older_than_time)
// 		);
// 		if ($safemode) {
// 			foreach($transients as $transient) {
// 				get_transient($transient);
// 			}
// 		} else {
// 			$options_names = array();
// 			foreach($transients as $transient) {
// 				$options_names[] = '_transient_' . $transient;
// 				$options_names[] = '_transient_timeout_' . $transient;
// 			}
// 			if ($options_names) {
// 				$options_names = array_map(array($wpdb, 'escape'), $options_names);
// 				$options_names = "'". implode("','", $options_names) ."'";
				
// 				$result = $wpdb->query( "DELETE FROM {$wpdb->options} WHERE option_name IN ({$options_names})" );
// 				if (!$result) {
// 					return false;
// 				}
// 			}
// 		}
// 		return $transients;
// 	}
// }
// function purge_transients_activation () {
// 	if (!wp_next_scheduled('purge_transients_cron')) {		
// 		wp_schedule_event( time(), 'daily', 'purge_transients_cron');
// 	}
// }
// register_activation_hook(__FILE__, 'purge_transients_activation');
// function purge_transients_deactivation () {
// 	if (wp_next_scheduled('purge_transients_cron')) {
// 		wp_clear_scheduled_hook('purge_transients_cron');
// 	}
// }
// register_deactivation_hook(__FILE__, 'purge_transients_deactivation');
// function do_purge_transients_cron () {
// 	purge_transients();
// }
// add_action('purge_transients_cron', 'do_purge_transients_cron');


//
// 
// add related posts to context
// 
// 
add_filter( 'timber_context', 'fancySquares_get_related_posts'  );

function fancySquares_get_related_posts( $context ) {
    $context['fancySquaresRelatedPosts'] = Timber::get_posts( fancySquares_set_related_posts() );
    return $context;
}


function fancySquares_set_related_posts()
{
  global $post;

  // Geting the current post's category lists if exists.
  $postcat = get_the_category( $post->ID ); 
  $category_ids = $postcat[0]->term_id;

  // Geting the current post's tags lists if exists.
  $post_tags = wp_get_post_tags($post->ID, array( 'fields' => 'ids' ));

  $related_post = array(
    'post_type' =>'post',
    'category__in' => $category_ids,
    'tag__in' => $post_tags,
    'posts_per_page'=>3,
    'order' => 'DESC',
    'post__not_in' => array($post->ID)
  );

  return $related_post;

}


//
// 
// add thumbnail to rss
// 
// 
function rmh_add_image_to_rss($content) {   
    if(is_feed()) {   
        $post_id = get_the_ID();   
         
        $rssimage = get_the_post_thumbnail($post_id, 'thumbnail'); 
 
            if( !empty($rssimage) ):  
 
                $output .= '<p class="rss-image">'; 
                $output .=  $rssimage; 
                $output .= '</p>';  
            endif;  
   
        $content = $output;   
    }   
    return $content;   
}   
add_filter('the_content','rmh_add_image_to_rss');


function prefix_set_feed_cache_time( $seconds ) {
  return 1;
}
add_filter( 'wp_feed_cache_transient_lifetime' , 'prefix_set_feed_cache_time' );