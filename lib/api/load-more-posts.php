<?php

add_action('wp_ajax_more_all_posts', __NAMESPACE__ . '\\ajax_more_all_posts');
add_action('wp_ajax_nopriv_more_all_posts', __NAMESPACE__ . '\\ajax_more_all_posts');
function ajax_more_all_posts()
{ 

    $cat_id = $_REQUEST[ 'cat' ] or $_GET[ 'cat' ];
    $directory = isset($_REQUEST['directory']) ? $_REQUEST['directory'] : '';
    $postCount = $_POST['posts_per_page'];
    

    $args = [
        'cat' => $cat_id,
        'posts_per_page' => $postCount,
        'post_status' => 'publish'
    ];

    if($paged != ""){
        $args['paged'] = $paged; 
    }

    

    $posts = Timber::get_posts($args);
    $message = [];
    if($posts){
        $i = 0;
        foreach($posts as $post){

            $response = array(
                'data' => Timber::compile('templates/blocks/content-post.twig', ['post' => $post, 'directory' => $directory, 'author' => $author, 'ajaxloop' => $i++])
            );
            $message[] = $response;
        }
    } else {
        $message = "nomore";
    }
    wp_reset_query();
    wp_reset_postdata();

    wp_send_json($message);
    die();
}