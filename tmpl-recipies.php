<?php
/**
 * Template Name: Recipe Guide
 * Description: Recipe Guide Page Template - see pages and partials
 */
// $start = TimberHelper::start_timer();
$context = Timber::get_context();

    $timeOfDayTerms = get_terms( 'time_of_day', array(
        'orderby' => 'name',
        'order'   => 'ASC',
        'hide_empty' => '1'
    ) );
        
    $context['TOD_TERMS'] = $timeOfDayTerms;

    $dietTerms = get_terms( 'recipe_diet_type', array(
        'orderby' => 'name',
        'order'   => 'ASC',
        'hide_empty' => '1'
    ) );
        
    $context['DIET_TERMS'] = $dietTerms;
    
Timber::render('pages/recipe-guide.twig', $context);